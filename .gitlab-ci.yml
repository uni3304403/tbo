stages:
  - test
  - build

variables:
  PROJECT_DIR: src
  NVD_CACHE_KEY: nvd
  NVD_CACHE_PATH: owasp/data
  PYTHON_IMAGE: python:3.11-alpine
  OWASP_IMAGE: owasp/dependency-check:9.0.7
  ZAP_IMAGE: owasp/zap2docker-stable:2.14.0
  DOCKER_IMAGE: docker:20.10.16
  DOCKER_DIND_IMAGE: docker:20.10.16-dind
  DOCKER_HOST: tcp://docker:2376
  DOCKER_TLS_CERTDIR: "/certs"
  SECRET_DETECTION_REPORT_FILE: "gl-secret-detection-report.json"
  SAST_REPORT_FILE: "gl-sast-report.json"

include:
  - template: Jobs/SAST.gitlab-ci.yml # Static Application Security Testing
  - template: Jobs/Secret-Detection.gitlab-ci.yml # Secret Detection

unit-test:
  stage: test
  allow_failure: false
  image: $PYTHON_IMAGE
  script:
    - cd $PROJECT_DIR
    - pip install -r requirements.txt
    - python -m unittest

nodejs-scan-sast:
  stage: test
  allow_failure: false
  artifacts:
    paths:
      - $SAST_REPORT_FILE
    expire_in: 1 hour

nodejs-scan-sast_eval:
  stage: test
  allow_failure: true
  needs:
    - job: nodejs-scan-sast
  variables:
    GIT_STRATEGY: none
  before_script:
    - apt-get update -qy
    - apt-get install -y jq
  script:
    - |
      du -h $SAST_REPORT_FILE
      if [ -f "$SAST_REPORT_FILE" ]; then
        if [ "$(jq ".vulnerabilities | length" $SAST_REPORT_FILE)" -gt 0 ]; then
          echo "Vulnerabilities detected. Please analyze the artifact $SAST_REPORT_FILE produced by the 'nodejs-scan-sast' job."
          exit 80
        fi
      else
        echo "Artifact $SAST_REPORT_FILE does not exist. The 'nodejs-scan-sast' job likely didn't create one. Hence, no evaluation can be performed."
      fi

semgrep-sast:
  stage: test
  allow_failure: false
  artifacts:
    paths:
      - $SAST_REPORT_FILE
    expire_in: 1 hour

semgrep-sast_eval:
  stage: test
  allow_failure: true
  needs:
    - job: semgrep-sast
  variables:
    GIT_STRATEGY: none
  before_script:
    - apt-get update -qy
    - apt-get install -y jq
  script:
    - |
      du -h $SAST_REPORT_FILE
      if [ -f "$SAST_REPORT_FILE" ]; then
        if [ "$(jq ".vulnerabilities | length" $SAST_REPORT_FILE)" -gt 0 ]; then
          echo "Vulnerabilities detected. Please analyze the artifact $SAST_REPORT_FILE produced by the 'semgrep-sast' job."
          exit 80
        fi
      else
        echo "Artifact $SAST_REPORT_FILE does not exist. The 'semgrep-sast' job likely didn't create one. Hence, no evaluation can be performed."
      fi

secret_detection:
  stage: test
  allow_failure: false
  artifacts:
    paths:
      - $SECRET_DETECTION_REPORT_FILE
    expire_in: 1 hour

secret_detection_eval:
  stage: test
  allow_failure: true
  needs:
    - job: secret_detection
  variables:
    GIT_STRATEGY: none
  before_script:
    - apt-get update -qy
    - apt-get install -y jq
  script:
    - |
      if [ -f "$SECRET_DETECTION_REPORT_FILE" ]; then
        if [ "$(jq ".vulnerabilities | length" $SECRET_DETECTION_REPORT_FILE)" -gt 0 ]; then
          echo "Vulnerabilities detected. Please analyze the artifact $SECRET_DETECTION_REPORT_FILE produced by the 'secret-detection' job."
          exit 80
        fi
      else
        echo "Artifact $SECRET_DETECTION_REPORT_FILE does not exist. The 'secret-detection' job likely didn't create one. Hence, no evaluation can be performed."
      fi

pyupio-sca:
  stage: test
  allow_failure: true
  variables:
    REPORT_NAME: pyupio-sca_report.json
  image: $PYTHON_IMAGE
  script:
    - cd $PROJECT_DIR
    - pip install safety
    - safety check --json -r requirements.txt > $REPORT_NAME
  after_script:
    - cp $PROJECT_DIR/$REPORT_NAME .
  artifacts:
    when: always
    expire_in: 1 week
    paths:
      - $REPORT_NAME

owasp-download-data:
  stage: test
  cache:
    key: $NVD_CACHE_KEY
    paths:
      - $NVD_CACHE_PATH
  image:
    name: $OWASP_IMAGE
    entrypoint: [""]
  script:
    - /usr/share/dependency-check/bin/dependency-check.sh --updateonly --data $NVD_CACHE_PATH

owasp-sca:
  stage: test
  allow_failure: true
  needs:
    - job: owasp-download-data
  cache:
    key: $NVD_CACHE_KEY
    paths:
      - $NVD_CACHE_PATH
  image:
    name: $OWASP_IMAGE
    entrypoint: [""]
  script:
    - test -d $NVD_CACHE_PATH
    - /usr/share/dependency-check/bin/dependency-check.sh --scan $PROJECT_DIR --data $NVD_CACHE_PATH --format ALL --project $CI_PROJECT_NAME --enableExperimental --failOnCVSS 7 --noupdate
  artifacts:
    when: always
    expire_in: 1 week
    paths:
      - dependency-check-report.html

zap-dast:
  stage: test
  allow_failure: true
  variables:
    REPORT_NAME: zap_report.html
  image: $ZAP_IMAGE
  script:
    - cd $PROJECT_DIR
    - pip install -r requirements.txt
    - python app.py &
    - mkdir /zap/wrk
    - /zap/zap-full-scan.py -t http://127.0.0.1:5000/ -g gen.conf -r $REPORT_NAME
  after_script:
    - cp /zap/wrk/$REPORT_NAME .
  artifacts:
    when: always
    expire_in: 1 week
    paths:
      - $REPORT_NAME

build:
  stage: build
  variables:
    CONTAINER_IMAGE_TAG: $CI_COMMIT_REF_SLUG
  image: $DOCKER_IMAGE
  services:
    - $DOCKER_DIND_IMAGE
  script:
    - cd $PROJECT_DIR
    - docker build --pull -t $CI_REGISTRY_IMAGE:$CONTAINER_IMAGE_TAG .
    - docker push $CI_REGISTRY_IMAGE:$CONTAINER_IMAGE_TAG
  before_script:
    - docker login -u $CI_REGISTRY_USER -p $CI_REGISTRY_PASSWORD $CI_REGISTRY
  rules:
    - if: $CI_COMMIT_REF_NAME == $CI_DEFAULT_BRANCH
      variables:
        CONTAINER_IMAGE_TAG: latest
    - if: $CI_COMMIT_REF_NAME != $CI_DEFAULT_BRANCH
      variables:
        CONTAINER_IMAGE_TAG: beta
  when: manual
