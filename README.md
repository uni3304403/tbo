# TBO

## Definicja procesu CI/CD

Zaprojektowany proces CI/CD składa się z dwóch etapów:

- test: pierwszy etap, uruchamiany automatycznie, obejmuje testy jednostkowe, detekcję sekretów, testy typu SCA, testy typu SAST oraz testy typu DAST.
- build: drugi etap procesu, uruchamiany ręcznie po weryfikacji wyników etapu pierwszego. Buduje obraz dockerowy z aplikacją, a następnie wypycha go do publicznego rejestru dockerowego *docker.io*.

### Etap *test*

- Testy jednostkowe:
    - unit-test - uruchomienie testów jednostkowych z użyciem *pytest*, w przypadku niepowodzenia job zwraca kod błędu. 
- Detekcja sekretów:
    - secret_detection - weryfikuje sekrety umieszczone w kodzie źródłowym przy użyciu standardowego szablonu GitLab-owego. Zawsze kończy się sukcesem, zwraca artefakt - raport/plik w formacie *json*.
    - secret_detection_eval - automatycznie analizuje artefakt z secret_detection, kończy się niepowodzeniem w przypadku, jeśli raport zawiera informację o wykrytych sekretach.
- SAST:
    - nodejs-scan-sast, semgrep-sast - dokonują statycznej analizy kodu źródłowgo przy użyciu standardowego szablonu GitLab-owego. Zawsze kończy się sukcesem, zwraca artefakt - raport/plik w formacie *json*.
    - nodejs-scan-sast_eval, semgrep-sast_eval - automatycznie analizują artefakty z nodejs-scan-sast oraz semgrep-sast, kończą się niepowodzeniem w przypadku, jeśli dany raport zawiera jakieś wykryte podatności.
- SCA:
    - pyupio-sca - dokonuje analizy składników oprgoramowania przy użyciu standarodowego szablonu GitLab-owego.
    - owasp-download-data - w razie potrzeby pobiera najnowszą bazę danych *NVD* do pamięci podręcznej.
    - owasp-sca - dokonuje analizy składników oprgoramowania, kończy się niepowodzeniem w przypadku wykrycia podatności o poziomie co najmniej 7.
- DAST:
    - zap-dast - dokonuje dynamicznej analizy kodu źródłowgo na uruchomionej aplikacji przy użyciu standardowego szablonu GitLab-owego.

Skanery bezpieczeństwa wykorzystane w procesie CI/CD:

- GitLeaks [^gitleaks]
- Semgrep [^semgrep]
- njsscan [^njsscan]
- pyupio [^pyupio]
- OWASP Dependency-Check [^owasp]
- Zed Attack Proxy (ZAP) [^zaproxy]

### Etap *build*

Uruchamiany ręcznie po weryfikacji wyników poprzedniego etapu. Zawiera tylko jeden job:

- build - tworzy obraz Dockerowy z tagiem *latest* z gałęzi *main*, lub z tagiem *beta* z innych gałęzi repozytorium. W przypadku powodzenia wypycha zbudowany obraz do publicznego rejestru obrazów Dockerowych *docker.io*.

[^gitleaks]: https://gitleaks.io
[^semgrep]: https://semgrep.dev
[^njsscan]: https://github.com/ajinabraham/njsscan
[^pyupio]: https://semgrep.dev
[^owasp]: https://owasp.org/www-project-dependency-check

[^zaproxy]: https://www.zaproxy.org
